﻿
# Modify the get_next_target procedure so that
# if there is a link it behaves as before, but
# if there is no link tag in the input string,
# it returns None, 0.

# Note that None is not a string and so should
# not be enclosed in quotes.

# Also note that your answer will appear in
# parentheses if you print it.

def get_next_target(page):
    start_link = page.find('<a href=')

    if start_link == -1:  
        return None, 0
    start_quote = page.find('"', start_link)
    end_quote = page.find('"', start_quote + 1)
    url = page[start_quote + 1:end_quote]
    return url, end_quote
    
    
def print_all_links(page):    
    while True:
        url, endpos = get_next_target(page)
        if url:
            print url
            page =  page[endpos:]
        else:
            break
            
            
def get_all_links(page): 
    links = []
    while True:
        url, endpos = get_next_target(page)
        if url:
            links.append(url)
            page = page[endpos:]
        else:
            break
    return url
    
def crawl_web(seed):
    tocrawl = [seed]
    crawled = []
    while tocrawl:
        page = tocrawl.pop()
        if page not in crawled:            
            crawled.append(page)
            links = get_all_links(page) #Depth-First Search
            for i in links:
                tocrawl.append(i)
    return crawled            
                
            
print get_next_target('hi this is a "string" with no link')
print get_next_target('this is a <a href="http://udacity.com">link!</a>')
