﻿# Define a procedure, greatest,
# that takes as input a list
# of positive numbers, and
# returns the greatest number
# in that list. If the input
# list is empty, the output
# should be 0.

def greatest(num_list):
    if len(num_list) == 0:
        return 0
    i = 0
    j = 0
    while i < len(num_list):
        while j < len(num_list):
            if num_list[i] < num_list[j]:
                num_list[i],num_list[j] = num_list[j],num_list[i]
            j = j + 1
        i = i + 1
    return num_list[0]

def greatest1(num_list):
   big = 0
   for p in num_list:
       if p > big:
           big = p
   return big
                    




print greatest([4,23,1])
#>>> 23
#print greatest([])
#>>> 0

    
