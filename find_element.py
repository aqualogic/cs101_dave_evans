﻿# Define a procedure, find_element,
# that takes as its inputs a list
# and a value of any type, and
# returns the index of the first
# element in the input list that
# matches the value.

# If there is no matching element,
# return -1.

def find_element(l,t):
    pos = -1    
    found = False
    for i in l: 
        pos = pos + 1
        if i == t:  
            found = True
            break
    if found:
        return pos
    else:
        return -1
        
def find_element1(l,t):   
        i = 0
        while i < len(p):
            if p[i] == t:
                return i
            i = i + 1
        return -1

def find_element2(l,t):
    i = 0
    for e in l:
        if e == t:
            return i
        i = i + 1
    return -1

def find_element3(l,t):
    return l.index(t) # will not return -1
    
def find_element4(l,t):        
    if t in l:
        return l.index(t)
    return -1

print find_element([1,3,3],3)
#>>> 2

print find_element(['alpha','beta'],'gamma')
#>>> -1