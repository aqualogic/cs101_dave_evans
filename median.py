﻿# Define a procedure, median, that takes three
# numbers as its inputs, and returns the median
# of the three numbers.

# Make sure your procedure has a return statement.

def bigger(a,b):
    if a > b:
        return a
    else:
        return b

def biggest(a,b,c):
    return bigger(a,bigger(b,c))

def median(a,b,c):
    n1 = bigger(a,b)
    n2 = bigger(b,c)
   # print n1,n2
    if n1 == n2:
        if a > c:
            return a
        else:
            return c
    if n1 > n2:
        return n2
    else:
        return n1
        
def median1(a,b,c):
    big = biggest(a,b,c)
    if a==big:
        return bigger(b,c)
    if b == big:
        return bigger(a,c)
    else:
        return bigger(a,b)

#n1=3, n2=2, 3 1 2


print(median(2,1,3))

print(median(1,3,2))
#>>> 2

print(median(9,3,6))
#>>> 6

print(median(7,8,7))
#>>> 7
print (median(10,9,8))







